package com.retail.core;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

// Writing a parameterized test for Air Cost Calculator
@RunWith(Parameterized.class)
public class AirCostCalculatorTest {
	private double expectedValue;
	private Item item;
	

	public AirCostCalculatorTest(double expectedValue, Item item) {
		super();
		this.expectedValue = expectedValue;
		this.item = item;
	}

	AirCostCalculator airCostCalculator;
	@Before
	public void setup() {
		airCostCalculator = new AirCostCalculator();
	}
	@Test
	 public void testCalculateShippingCost() {
		assertEquals(expectedValue, airCostCalculator.calculateShippingCost(item),0.001);
	}

	@Parameters
	public static Collection<Object[]> testData() {
		Object[][] data = new Object[][] { {4.63, new Item(567321101987L, "CD � Pink Floyd, Dark Side Of The Moon", 19.99, 0.58, "AIR")},
			{4.4, new Item(567321101985L, "CD � Queen, A Night at the Opera", 20.49, 0.55, "AIR")}};
			return Arrays.asList(data);
	}
}
