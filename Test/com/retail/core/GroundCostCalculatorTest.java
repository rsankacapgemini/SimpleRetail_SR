package com.retail.core;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class GroundCostCalculatorTest {

	GroundCostCalculator groundCostCalculator;
	@Before
	public void setup() {
		groundCostCalculator = new GroundCostCalculator();
	}
	
	@Test
	public void calculateShippingCostTest() {
		assertEquals(1.52, groundCostCalculator.calculateShippingCost(new Item(567321101986L, "CD � Beatles, Abbey Road", 17.99, 0.61, "GROUND")),0.001);
	}
	
}
