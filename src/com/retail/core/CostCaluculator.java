package com.retail.core;

import java.util.ArrayList;
import java.util.Map;

public interface CostCaluculator {
	 double calculateShippingCost(Item item);
}
