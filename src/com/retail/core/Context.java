package com.retail.core;

public class Context {
	private CostCaluculator costCaluculator;

	public Context(CostCaluculator costCaluculator) {
		super();
		this.costCaluculator = costCaluculator;
	}
	
	public double calculateShippingCost(Item item) {
		return costCaluculator.calculateShippingCost(item);
	}
	

}
