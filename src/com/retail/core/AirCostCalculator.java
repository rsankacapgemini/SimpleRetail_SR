package com.retail.core;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AirCostCalculator implements CostCaluculator {

	HashMap<Long, Double> costMap = new HashMap<>();
	@Override
	public double calculateShippingCost(Item item) {
		// TODO Auto-generated method stub
		long upc=item.getUpc();
		upc=upc/10;
		int multiplier = (int) (upc%10);
		double cost = item.getWeight()*multiplier;
		//cost = cost+(0.03*item.getPrice());
		cost =Math.floor(cost* 100) / 100;
		return cost;
	}

}
